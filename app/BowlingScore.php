<?php
/**
 * Created by PhpStorm.
 * User: kianxiong.foo
 * Date: 20/10/2017
 * Time: 5:51 PM
 */

namespace App;


use Mockery\CountValidator\Exception;

class BowlingScore
{
    public $arr_frames;
    public $arr_scores = array();

    public function __construct()
    {
        //Nothing here please proceed
    }

    /**
     * This function convert the string of frames into an array
     * @param $str_frames
     * @return array
     */
    public function convertFramesToArray($str_frames)
    {
        //remove any spaces
        $str_frames = str_replace(" ", "", $str_frames);

        //validation
        //Since we will only have 10 frames and it is fixed
        if (substr_count($str_frames, "[[") != 1 ||
            substr_count($str_frames, "]]") != 1 ||
            substr_count($str_frames, "],[") != 9
        ) {
            throw new Exception('Invalid Input. It is not an array of array.');
        };

        //remove first 2 and last 2 character
        $str_frames = substr($str_frames, 2, -2);

        //split each frame by |
        $str_frames = str_replace("],[", "|", $str_frames);

        //insert each frame to array
        $this->arr_frames  = explode("|", $str_frames);

        return $this->arr_frames;
    }

    /**
     * This function will calculate the output scores.
     * @param $arr_frames
     * @return array
     */
    public function getScores($arr_frames)
    {
        $sum = 0;
        $strike_flag = false;
        $strike_counter = 0;

        foreach ($arr_frames as $key => $frame) {
            //if strike
            if ($frame == '10') {
                $strike_flag = true;
                $strike_counter++;
            //if not a strike
            } else {
                //split the frame into array
                $frame = explode(",", $frame);

                //make sure all inputs are numbers
                foreach ($frame as $frame_num) {
                    if (!is_numeric($frame_num)) {
                        throw new Exception('Invalid Input. '. $frame_num . ' is not an integer!');
                    }
                }

                //sum up scores of 2 throws
                $frame = array_sum($frame);

                //if there is any strike before this frame
                if ($strike_flag == true) {

                    //only if more than 1 strike
                    if ($strike_counter > 1) {
                        //first push the full strike score
                        $sum = $strike_counter * 10 + $sum;
                        array_push($this->arr_scores, $sum);
                        $strike_counter--;
                    }
                    //push the rest of the strike score
                    for ($i = $strike_counter; $i > 0 ; $i--) {
                        $sum = $i * 10 + $frame + $sum;
                        array_push($this->arr_scores, $sum);
                    }

                    //current frame
                    $sum = $sum + $frame;
                    array_push($this->arr_scores, $sum);

                    //reset counter and flag
                    $strike_flag = false;
                    $strike_counter = 0;
                } else { //if there is no any strike before this frame
                    $sum = $sum + $frame;
                    array_push($this->arr_scores, $sum);
                }
            }
        }

        return $this->arr_scores;
    }



}
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\BowlingScore;
use Mockery\CountValidator\Exception;

class BowlingGame extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bowling_game:start {str_frames}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Accepts array of an array of bowling frames and calculate the score.';
    

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $str_frames = $this->argument('str_frames');

            $obj_scores = new BowlingScore();
            $arr_frames = $obj_scores->convertFramesToArray($str_frames);
            $arr_scores = $obj_scores->getScores($arr_frames);

            $this->info("Outputs: ". implode(",", $arr_scores));
        } catch (Exception $e) {
            $this->error('Caught exception: '.  $e->getMessage(). "\n");
        }

    }
}

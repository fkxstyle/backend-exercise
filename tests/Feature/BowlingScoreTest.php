<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\BowlingScore;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class BowlingScoreTest extends TestCase
{
    /**
     * Test either the string frames is splitted into array successfully
     *
     * @return void
     */
    public function testConvertFramesToArray()
    {
        $str_frames = "[[5,2], [8,1], [6,4],[10], [0,5],[2,6],[8,1],[5,3],[6,1],[10,2,6]]";

        $obj_scores = new BowlingScore();
        $arr_frames = $obj_scores->convertFramesToArray($str_frames);

        $this->assertEquals("5,2", $arr_frames[0]);
        $this->assertEquals("10", $arr_frames[3]);
        $this->assertEquals("10,2,6", $arr_frames[9]);
    }

    /**
     * Some of the test case result is based on http://www.bowlinggenius.com/
     */
    public function testGetScores()
    {
        //single strike case
        $str_frames = "[[5,2], [8,1], [6,4],[10], [0,5],[2,6],[8,1],[5,3],[6,1],[10,2,6]]";
        $arr_response = array(7,16,26,41,46,54,63,71,78,96);
        $obj_scores = new BowlingScore();
        $arr_frames = $obj_scores->convertFramesToArray($str_frames);
        $arr_scores = $obj_scores->getScores($arr_frames);

        $this->assertEquals($arr_scores, $arr_response);


        //no strike case
        $str_frames = "[[1,2], [1,2], [1,2],[1,2], [1,2],[1,2],[1,2],[1,2],[1,2],[1,2]]";
        $arr_response = array(3,6,9,12,15,18,21,24,27,30);
        $obj_scores = new BowlingScore();
        $arr_frames = $obj_scores->convertFramesToArray($str_frames);
        $arr_scores = $obj_scores->getScores($arr_frames);

        $this->assertEquals($arr_scores, $arr_response);


        //first 3 strike in a row case
        $str_frames = "[[10], [10], [10],[5,2], [0,5],[2,6],[8,1],[5,3],[6,1],[10,2,6]]";
        $arr_response = array(30,57,74,81,86,94,103,111,118,136);
        $obj_scores = new BowlingScore();
        $arr_frames = $obj_scores->convertFramesToArray($str_frames);
        $arr_scores = $obj_scores->getScores($arr_frames);

        $this->assertEquals($arr_scores, $arr_response);


        //3 strike in a row on a second frame case
        $str_frames = "[[0,9], [10], [10],[10], [5,0],[2,6],[8,1],[5,3],[6,1],[10,2,6]]";
        $arr_response = array(9,39,64,79,84,92,101,109,116,134);
        $obj_scores = new BowlingScore();
        $arr_frames = $obj_scores->convertFramesToArray($str_frames);
        $arr_scores = $obj_scores->getScores($arr_frames);

        $this->assertEquals($arr_scores, $arr_response);


        //1 strike on second frame and 2 strike in a row on the last frame case
        $str_frames = "[[0,9], [10], [0,9],[0,9], [5,0],[2,6],[8,1],[5,3],[6,1],[10,10,6]]";
        $arr_response = array(9,28,37,46,51,59,68,76,83,109);
        $obj_scores = new BowlingScore();
        $arr_frames = $obj_scores->convertFramesToArray($str_frames);
        $arr_scores = $obj_scores->getScores($arr_frames);

        $this->assertEquals($arr_scores, $arr_response);
    }
}

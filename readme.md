Some of the test case results are based on http://www.bowlinggenius.com/ with no additional score for a spare. 

Unit test is located at:
 tests / Feature / BowlingScoreTest.php

Example of execution:
php artisan bowling_game:start "[[5,2], [8,1], [6,4],[10], [0,5],[2,6],[8,1],[5,3],[6,1],[10,2,6]]"
